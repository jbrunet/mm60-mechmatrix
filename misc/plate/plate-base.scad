
$fn = 32;

aLot = 1000;

cherry_width = 19.05;
cherry_base = 4.8;
plate_height = 1.9 ;
pilotis_height = cherry_base - plate_height;
pilotis_diam = 3;

module pilotis_base() {
    translate([0, cherry_width, 0]) {
        cylinder(d = pilotis_diam, cherry_base);
        translate([0, cherry_width, 0]) {
            cylinder(d = pilotis_diam, cherry_base);
            translate([0, cherry_width, 0]) {
                cylinder(d = pilotis_diam, cherry_base);
            }
        }
    }   
}

module pilotis() {
    translate([0, cherry_width, 0]) {
        cylinder(d = pilotis_diam, cherry_base);
        pilotis_base();
    }
}

module pilotis_space() {
    translate([0, cherry_width, 0]) {
        pilotis_base();
    }
}

module plate_base() {
    union() {
        linear_extrude(height = plate_height)
            import(file = "plate-base.dxf");

        translate([0, 0, plate_height - cherry_base]) {
        translate([2, 5, 0])
            cube([3, 29 , cherry_base]);
        
        translate([281, 19, 0])
            cube([3, 15 , cherry_base]);    
        
        translate([2, 43, 0])
            cube([3, 48 , cherry_base]);
        
        translate([281, 43, 0])
            cube([3, 48 , cherry_base]);
        
        translate([1.4 * cherry_width, 0]) {
            pilotis();
        }
     
        translate([2.5 * cherry_width, 0]) {
            pilotis();
        }    

        translate([3.5 * cherry_width, 0]) {
            pilotis();
        }
        
        translate([4.5 * cherry_width, 0]) {
            pilotis();
        }
        
        translate([5.5 * cherry_width, 0]) {
            pilotis();
        }
        
        translate([6.45 * cherry_width, 0]) {
            pilotis_space();
        }
            
        translate([7.5 * cherry_width, 0]) {
            pilotis_space();
        }
           
        translate([8.5 * cherry_width, 0]) {
            //Remove on cut
            //pilotis();
        }
           
        translate([9.5 * cherry_width, 0]) {
            pilotis();
        }
           
        translate([10.5 * cherry_width, 0]) {
            pilotis();
        }
           
        translate([11.5 * cherry_width, 0]) {
            pilotis();
        }
           
        translate([12.5 * cherry_width, 0]) {
            pilotis();
        }
           
        translate([13.6 * cherry_width, 0]) {
            pilotis();
        }
        
        }
    }
}
divide = 8.5;
spacing = 0.15;

module cube_left(margin) {
    color([1.0, 0, 0, 0.3])
        cube([2 * (divide * cherry_width + margin) ,aLot,aLot],center = true);
}


module cube_support() {
    l = 17.5;
    h = 14.6;
    p = 2.2;
    color([0, 1.0, 0, 0.8]) {
        cube([h, l, plate_height], center=true);
        translate([h/4, l/4, 0])
            cube([p, l/2, aLot], center=true);
        translate([-h/4, l/4, 0])
            cube([p, l/2, aLot], center=true);
    }
}

module plate() {
    difference() {
        plate_base();
        
        // Make more space for tabs
        translate([148.4, 10.25, 0])
            cube_support();
        translate([117.6, 10.25, 0])
            cube_support();
    }
}

plate();
cube_left(0);

//cube_support();